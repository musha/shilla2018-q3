require(["app", "managers/layout-manager", "managers/router", "managers/resize-manager", "managers/tracker", "managers/ui-framework7"],


    function(app, LayoutManager, Router, ResizeManager, Tracker, Framework7) {

        // layout manager
        app.layout = (new LayoutManager()).layout();

        // router
        app.router = new Router();

        app.resizeManager = new ResizeManager();

        app.framework7 = new Framework7();

        app.tracker = new Tracker({ gaAccount: "", baiduAccount: "" });

        app.eventBus = _.extend({}, Backbone.Events);

        // 题目组
        app.qArr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
        // 结果
        app.result = '';

        // 接口地址
        // app.baseUrl = 'http://campaign.lancome.com.cn/genshenzhen2018';

        /*
         app.user = new User.Model();
         app.user.getInfo();
         */

        app.layout.render().promise().done(function() {
            Backbone.history.start({
                pushState: false
            });
        });

        // 微信不分享
        // function onBridgeReady() { 
        //     WeixinJSBridge.call('hideOptionMenu'); 
        // };
        // if (typeof WeixinJSBridge == "undefined") { 
        //     if (document.addEventListener) { 
        //         document.addEventListener('WeixinJSBridgeReady', onBridgeReady, false); 
        //     } else if (document.attachEvent) { 
        //         document.attachEvent('WeixinJSBridgeReady', onBridgeReady);
        //         document.attachEvent('onWeixinJSBridgeReady', onBridgeReady); 
        //     } 
        // } else { 
        //     onBridgeReady(); 
        // };

        //微信分享
        var shareUrl = "http://campaign.archisense.cn/ShillaTravelQuiz";


        app.shareDataFriend = {
            share: [
                {
                    title: '挑对合拍旅行地，放飞自己很容易！',
                    desc: ' 三分钟答题，解锁你的旅行合拍地！',
                    link: shareUrl,
                    imgUrl: "http://campaign.archisense.cn/ShillaTravelQuiz/assets/images/share.jpg",
                    success: function() {
                        gtag('event', 'share_friend', {'event_category': 'shilla2018Q3', 'event_label': 'click' });
                        // app.tracker.event('shared', 'event', 'share');

                    }
                },
                {
                    title: '“十一”旅行还没定？新罗帮你安排上！',
                    desc: ' 三分钟答题，解锁你的旅行合拍地！',
                    link: shareUrl,
                    imgUrl: "http://campaign.archisense.cn/ShillaTravelQuiz/assets/images/share.jpg",
                    success: function() {
                        gtag('event', 'share_friend', {'event_category': 'shilla2018Q3', 'event_label': 'click' });
                        // app.tracker.event('shared', 'event', 'share');

                    }
                },
                {
                    title: '没想到我是这样的旅行家…',
                    desc: ' 三分钟答题，解锁你的旅行合拍地！',
                    link: shareUrl,
                    imgUrl: "http://campaign.archisense.cn/ShillaTravelQuiz/assets/images/share.jpg",
                    success: function() {
                        gtag('event', 'share_friend', {'event_category': 'shilla2018Q3', 'event_label': 'click' });
                        // app.tracker.event('shared', 'event', 'share');

                    }
                }
            ]
        };
        app.shareDataTimeLine = {
            share: [
                {
                    title: '挑对合拍旅行地，放飞自己很容易！',
                    desc: ' 三分钟答题，解锁你的旅行合拍地！',
                    link: shareUrl,
                    imgUrl: "http://campaign.archisense.cn/ShillaTravelQuiz/assets/images/share.jpg",
                    success: function() {
                        gtag('event', 'share_moment', {'event_category': 'shilla2018Q3', 'event_label': 'click' });
                        // app.tracker.event('shared', 'event', 'share');

                    }
                },
                {
                    title: '“十一”旅行还没定？新罗帮你安排上！',
                    desc: ' 三分钟答题，解锁你的旅行合拍地！',
                    link: shareUrl,
                    imgUrl: "http://campaign.archisense.cn/ShillaTravelQuiz/assets/images/share.jpg",
                    success: function() {
                        gtag('event', 'share_moment', {'event_category': 'shilla2018Q3', 'event_label': 'click' });
                        // app.tracker.event('shared', 'event', 'share');

                    }
                },
                {
                    title: '没想到我是这样的旅行家…',
                    desc: ' 三分钟答题，解锁你的旅行合拍地！',
                    link: shareUrl,
                    imgUrl: "http://campaign.archisense.cn/ShillaTravelQuiz/assets/images/share.jpg",
                    success: function() {
                        gtag('event', 'share_moment', {'event_category': 'shilla2018Q3', 'event_label': 'click' });
                        // app.tracker.event('shared', 'event', 'share');

                    }
                }
            ]
        };

        if (window.location.host.indexOf("campaign.archisense.cn") != -1) {
            require(["//res.wx.qq.com/open/js/jweixin-1.0.0.js"],
            function(wx) {
                app.wx = wx;
                $.ajax({
                    url: '/wechat/api/share.php',
                    data: { url: document.location.href.split("#")[0] },
                    cache: false,
                    success: function(data) {
                        app.wx.config({
                            "debug": false,
                            "appId": data.appId,
                            "timestamp": data.timestamp,
                            "nonceStr": data.nonceStr,
                            "signature": data.signature,
                            "jsApiList": ["onMenuShareTimeline", "onMenuShareAppMessage", "onMenuShareQQ", "onMenuShareWeibo", "onMenuShareQZone"]
                        });
                        app.wx.ready(function() {
                            var shareNum = Math.floor(Math.random() * 3);
                            app.wx.onMenuShareAppMessage(app.shareDataFriend.share[shareNum]);
                            app.wx.onMenuShareTimeline(app.shareDataTimeLine.share[shareNum]);
                        });
                    },
                    error: function(XHR, textStatus, errorThrown) {
                    }
                });
            });
        }
    });