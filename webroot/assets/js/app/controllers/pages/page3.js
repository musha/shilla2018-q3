// Page module
define(["app", "controllers/base/page"],

    function(app, BasePage) {
        var Page = {};

        Page.View = BasePage.View.extend({
            fitOn: "width", //width, height, custom
            beforeRender: function() {
                var done = this.async();
                require(["vendor/zepto/zepto.html5Loader.min"],
                function() {
                    done();
                });
            },
            afterRender: function() {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
                // 调整手机屏幕尺寸 
                if ($(window).height() < 600) {
                    //
                } else {
                    // 
                };

                var context = this;

                //动画效果
                var tl = new TimelineMax();
                tl.from(context.$('.page3_page'), 0.4, { autoAlpha: 0, onComplete: function() {
                    dotTwinkle();
                    optionShake();
                } }, 0.1);

                // 提示
                function dotTwinkle() {
                    var tl = new TimelineMax({ repeat: -1, repeatDelay: 0.3 });
                    tl.to(context.$('.dot1'), 0.3, { autoAlpha: 1 });
                    tl.to(context.$('.dot2'), 0.3, { autoAlpha: 1 }, '+=0.1');
                    tl.to(context.$('.dot3'), 0.3, { autoAlpha: 1 }, '+=0.1');
                    tl.to(context.$('.dot'), 0.3, { autoAlpha: 0 }, '+=0.1');
                };

                // 选项晃动
                function optionShake() {
                    var tl = new TimelineMax({ });
                    tl.to(context.$('.btn-yes'), 0.5, { 'left': '19.4%', repeat: -1, yoyo: true, ease: Power0.easeNone }, 0);
                    tl.to(context.$('.btn-no'), 0.5, { 'left': '53%', repeat: -1, yoyo: true, ease: Power0.easeNone }, 0);
                };

                // 选yes
                $('.btn-yes').on('click', function() {
                    gtag('event', 'Q7_Y', {'event_category': 'shilla2018Q3', 'event_label': 'click' });
                    tl.kill();
                    if(app.qArr.length > 0) {
                        var qNum = app.randomQuestion();
                        app.router.goto('page' + app.qArr[qNum]);
                        app.qArr.splice(qNum, 1);
                    } else {
                        app.router.goto('result' + app.result);
                    } 
                });

                // 选no
                $('.btn-no').on('click', function() {
                    gtag('event', 'Q7_N', {'event_category': 'shilla2018Q3', 'event_label': 'click' });
                    tl.kill();
                    if(app.qArr.length > 0) {
                        var qNum = app.randomQuestion();
                        app.router.goto('page' + app.qArr[qNum]);
                        app.qArr.splice(qNum, 1);
                    } else {
                        app.router.goto('result' + app.result);
                    } 
                });
            },
            resize: function(ww, wh) {
                if ($(window).height() < 600) {
                    if (app.s >= 100) {
                        //
                    } else {
                        //
                    }
                } else {
                    if (app.s >= 100) {
                        //
                    } else {
                        //
                    }
                }
            },
            afterRemove: function() {},
        })
        //  Return the module for AMD compliance.
        return Page;
    })
